# Gecko BND RCP Exporter/Launcher

This aims toward enabling pure OSGi Projects and or bnd to better work with different specialties of Eclipse Equinox and the Eclipse RCP Framework. This entails:

* A BND Plugin, that allows product exports that use the native equinox launcher
* Splashscreen support
* In Framework start of the Eclipse Product/Application

!Please Note: This is explicitly no exact Product Export as the PDE would produce. The original Product export and Eclipse/Equinox start mechanism has a lot of non standard idiosyncrasies that have largely historic reasons and are nowadays unnecessary. Thus this product export works a bit different from what you currently know.! 

Everything described here will target the BND Workspace model. It should work with the maven model as well, but is untested at this point.

# HowTo

## BND Workspace template

Add the following repository as bnd workspace template: `https://gitlab.com/gecko.io/bnd-rcp-workspace-template.git`

How? open the Eclipse Preferences and go to: BNDTools -> "Workspace Template" and add it as a "Raw Git Clone URIs"  

## Get started

1.  Follow the steps here to get a preconfigured Eclipse installed: https://gitlab.com/gecko.io/bnd-workspace/-/wikis/HowTo (alternatively add the workspace template yourself. I needs to point here: https://gitlab.com/gecko.io/bnd-rcp-workspace-template.git)
2. Create a new Workspace using the Gecko RCP Workspace
3. Create a new bnd Project using the Gecko RCP Application template.
4. Have fun!

## Repositories

All Resources are provided as Maven Nexus and OBR repositories.

### Maven as BOMRepository

```
-plugin.geckorcp:\
	aQute.bnd.repository.maven.pom.provider.BndPomRepository;\
        name="Gecko Equinox RCP BOM";\
        releaseUrls="https://repo.maven.apache.org/maven2/";\
        snapshotUrls=https://devel.data-in-motion.biz/nexus/repository/dim-snapshot/;\
        revision=" org.geckoprojects.equinox:org.gecko.bnd.eclipse.bom:1.1.0"

```

### OBR

```
-plugin.geckorcpobr: \
	aQute.bnd.repository.osgi.OSGiRepository;\
		name="Gecko Equinox RCP OBR";\
		locations=https://devel.data-in-motion.biz/repository/gecko/release/geckoBNDEquinox/index.xml
```

## Bndrun configurations

| instruction      | Description                                                  |
| ---------------- | ------------------------------------------------------------ |
| `-launchericons` | A list of paths to one or more icon files. As this uses the default Eclipse launchable and mechanism it must either be one `.ico` file containing the following set of icons:<br />	 * 1. 256x256, 32 bit (RGB / Alpha Channel)<br/>	 * 2. 48x48, 32 bit (RGB / Alpha Channel)<br/>	 * 3. 32x32, 32 bit (RGB / Alpha Channel)<br/>	 * 4. 16x16, 32 bit (RGB / Alpha Channel)<br/>	 * 5. 48x48, 8 bit (256 colors)<br/>	 * 6. 32x32, 8 bit (256 colors)<br/>	 * 7. 16x16, 8 bit (256 colors)<br />Or 7 `.bmp` files in the above mentioned format, in any order.<br /><br />*!!!Providing the wrong files, can corrupt the launcher!!!*<br />**Icons are currently only supported for Windows exports** |

### Configuration Area

Eclipse provides a couple of possiblities to set the configuration area.

1. Edit the `-ini` and add 

   ``` 
   -configuration
   <myFancyPath>
   ```

2. Append the start command `myexecutable -configuration <myFancyPath>`
3. Set `-Dosgi.configuration.area=<myFancyPath> ` in the `.ini` or as java property
4. The OSGi default mechanism by setting the System or Framework property `org.osgi.framework.storage=<myFancyPath>`

The Launcher supports all options. If multiple options are set they will be evaluated in the given order, where 1 has the lowest priority and 4 the highest. Relative paths will be handled realtive to the install location.

***Please note that the use of the equinox variables like e.g. `@user.home` is currently not supported!*** 

### Splashscreen

The splash screen will now work with any OSGi Framework you want. the requirement the following bundle present on the runpath:
```
-runpath: \
	org.gecko.bnd.equinox.launcher.splashscreen
```
If our RCP Exporter is used, the native dll will be unpacked (if run from bnd or exported) and set via the `launcher.library` property. This needs to point to the necessary native `dll` or `so`. If this is present, the property `splash.location` needs to be present and point to a splash screen. If the splash is a relaitve path, it will first look for a splash realtive to where ever the java process is started from and if this fails relative to the install location.
If the argument `--nosplash` is given, the splashscreen will be supressed.

### Change the Launch mechanism

First of, how does launching work with BND in a nutshell? If you use the normal exporter, you get a self executable jar. This contains a Class that has a main method called the prelauncher. All your bundles are package the in the jar in a directory called jar. Besides the bundles from your bndrun, BND adds the biz.aQute.launcher as well. This jar is loaded by the prelauncher and the contained Launcher is triggered. This in turn creates and runs the actual Framework and installs and start all your bundles.

The original BND PreLauncher expects to to find the Launcher and bundles to be contained in its jar. The main difference is, that our PreLauncher can handle bundles in other locations. In addition to that we translate some eclipse specific properties as well.

If you want to use your own prelauncher, you can use the `-prelauncher: <somefilepath>`. Because the eclipse executable starts the PreLauncher, you need to provide a `Main-Class` Manifest header and a `public int run(String... args) throws Throwable` method in your Main Class.


## Product definition vs bndrun

This section will describe, how things you can do with the .product file can be done with bnd.

## Configure the about dialog

The PDE Product build itself doesn't do anything in regard to the about dialog. The magic happens the moment you click on the Synchronize link in the product editor. This simply updates the product defining plugin.xml. An example of your plugin xml would then look as follows:

```
      <product
            application="org.eclipse.e4.ui.workbench.swt.E4Application"
            name="test.app">
         <property
               name="applicationCSS"
               value="platform:/plugin/test.app/css/default.css">
         </property>
         <property
               name="appName"
               value="test.app">
         </property>
         <property
               name="aboutText"
               value="test">
         </property>
         <property
               name="aboutImage"
               value="test.png">
         </property>
      </product>
```


# BND Enablement ToDo List

## BND
* Support deflated Bundles in the Indexer
* Pack deflated jars on download if possible (need to check how p2 is doing this)

# Gecko Eclipse Core Supplement

If you want to use the Equinox registry without using the Equinox OSGi implementation, there is the bundle called *org.eclipse.equinox.supplement*. It bundles certain sources from the Equinox implementation, without having an OSGi framework.

The *org.gecko.eclipse.core.supplement* follows this approach and provides base services that are needed to get *org.eclipse.core* stuff running. In detail this is currently:

* **org.eclipse.osgi.environment.EnvironmentInfo** Service 
* **org.eclipse.osgi.service.localization.BundleLocalization** Service

Both interfaces are provided by either the  *org.eclipse.osgi* or *org.eclips.equinox.supplement* bundle.

To take profit of this extension you currently need modified version of some  bundles, mainly to avoid unnecessary dependencies.

## org.gecko.eclipse.core.supplement

This is an extension that provides the following services: 

* **org.eclipse.osgi.environment.EnvironmentInfo** Service 
* **org.eclipse.osgi.service.localization.BundleLocalization** Service

The *EnvironmentInfo* service is just an empty facade without deeper function. 

The *BundleLocalization* implementation is based upon that one from *org.eclipse.osgi.storage.BundleLocalizationImpl*. But the approach is more OSGi native and does not use Equinox internal stuff. This service is used to find property files for bundle translations. This service is working behind the *Platform#getResourceBundle* call in *org.eclipse.core.runtime*.

Nearly all EMF related projects use that.

To take profit of this extension you currently need modified version of some bundles, mainly to avoid unnecessary dependencies and hard links to the Equinox OSGi framework.

## org.eclipse.core.runtime

This bundle hard wires the Equinox OSGi framework as bundle requirement. That forces to use the Equinox OSGi implementation, whenever you need *org.eclipse.core.runtime*.

If you change the manifest and remove the require bundle declaration and use import packages instead, you open this bundle to run on other OSGi frameworks as well.

**Before**:

```
Require-Bundle: org.eclipse.osgi;bundle-version="[3.13.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.common;bundle-version="[3.10.0,4.0.0)";visibility:=reexport,
 org.eclipse.core.jobs;bundle-version="[3.10.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.registry;bundle-version="[3.8.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.preferences;bundle-version="[3.7.0,4.0.0)";visibility:=reexport,
 org.eclipse.core.contenttype;bundle-version="[3.7.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.app;bundle-version="1.3.0";visibility:=reexport
```



**After:**

```
Require-Bundle: org.eclipse.equinox.common;bundle-version="[3.10.0,4.0.0)";visibility:=reexport,
 org.eclipse.core.jobs;bundle-version="[3.10.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.registry;bundle-version="[3.8.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.preferences;bundle-version="[3.7.0,4.0.0)";visibility:=reexport,
 org.eclipse.core.contenttype;bundle-version="[3.7.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.app;bundle-version="1.3.0";visibility:=reexport
...
Import-Package: org.eclipse.equinox.log;version="1.1.0",
 org.eclipse.osgi.container;version="1.3.0",
 org.eclipse.osgi.framework.log;version="1.1.0",
 org.eclipse.osgi.service.datalocation;version="1.3.0",
 org.eclipse.osgi.service.debug;version="1.2.0",
 org.eclipse.osgi.service.environment;version="1.3.0",
 org.eclipse.osgi.service.resolver;version="1.6.0",
 org.eclipse.osgi.util;version="1.1.0",
 org.osgi.framework;version="1.9.0",
 org.osgi.framework.namespace;version="1.1.0",
 org.osgi.framework.wiring;version="1.2.0",
 org.osgi.resource;version="1.0.0",
 org.osgi.service.log;version="1.4.0",
 org.osgi.service.packageadmin;version="1.2.0",
 org.osgi.util.tracker;version="1.5.2"
```

Because many Eclispe plugins, ask the *org.eclipse.core.runtime.Platform*, if it is running. We also added a system property called **@noeclipse** and additionally evaluate it in the *org.eclipse.core.internal.runtime.InternalPlatform#isRunning*, which *Platform#isRunning* delegates to.

## org.eclipse.equinox.supplement

The sad thing is the heavy use of the static methods *Platform#getBundle* and *Platform#getBundles* in *org.eclipse.core.runtime*. The implementation in the *InternalPlatform#getBundles* uses plain OSGi except for one thing. 

It is the **ModuleContainer#createRequirement** call in *InternalPlatform#getBundles*. The creation of the *org.osgi.resource.Requirement* is the only real hard link to the internal Equinox OSGi framework in bundle *org.eclipse.osgi*. 

```java
Map<String, String> directives = Collections.singletonMap(Namespace.REQUIREMENT_FILTER_DIRECTIVE,
				getRequirementFilter(symbolicName, versionRange));
		Collection<BundleCapability> matchingBundleCapabilities = fwkWiring.findProviders(ModuleContainer
				.createRequirement(IdentityNamespace.IDENTITY_NAMESPACE, directives, Collections.emptyMap()));

```

Without that dependency, we can substitute the *org.eclipse.osgi* with *org.eclipse.equinox.supplement*. This opens.

So we moved this call to *org.eclipse.equinox.supplement*. All code in the supplement bundle is shared from other bundles, to avoid duplicating code. 

Despite of that we created a new package *org.eclipse.osgi.container* and put our own smaller implementation in there.

We also export that package in the manifest:

```
...
Export-Package: org.eclipse.equinox.log;version="1.1",
 org.eclipse.osgi.container;version="1.5.0",
 org.eclipse.osgi.framework.console;version="1.1",
 org.eclipse.osgi.framework.eventmgr;version="1.2",
 ...
```

... and also import the required packages:

```
...
Import-Package: org.osgi.framework,
 org.osgi.framework.hooks.resolver,
 org.osgi.resource,
 org.osgi.service.log,
 org.osgi.service.resolver,
 org.osgi.util.tracker,
 org.eclipse.equinox.log,
 org.eclipse.osgi.framework.console,
 ...
```

## Artifacts

The corresponding patches can be found in the *patch* folder. There are also patched version available from our nexus:

https://devel.data-in-motion.biz/nexus/repository/dim-release/

```
<dependency>
  <groupId>org.eclipse.core</groupId>
  <artifactId>org.eclipse.core.runtime</artifactId>
  <version>3.16.100.201911011611</version>
</dependency>

<dependency>
  <groupId>org.eclipse.equinox</groupId>
  <artifactId>org.eclipse.equinox.supplement</artifactId>
  <version>1.9.100.201911011403</version>
</dependency>

<dependency>
  <groupId>org.gecko.eclipse</groupId>
  <artifactId>org.gecko.eclipse.core.supplement</artifactId>
  <version>1.0.0</version>
</dependency>
```





