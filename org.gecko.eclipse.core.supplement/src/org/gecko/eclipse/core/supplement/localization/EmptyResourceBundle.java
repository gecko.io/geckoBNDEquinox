/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.eclipse.core.supplement.localization;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Implementation of an empty {@link ResourceBundle}, which has nothing and does nothing.
 * @author Mark Hoffmann
 * @since 07.11.2019
 */
public class EmptyResourceBundle extends ResourceBundle implements BundleResourceBundle {

	private static final String defaultRoot = Locale.ENGLISH.toString(); 
	private final String localeString;

	public EmptyResourceBundle(String locale) {
		super();
		this.localeString = locale;
	}

	public Enumeration<String> getKeys() {
		return Collections.enumeration(Collections.emptyList());
	}

	protected Object handleGetObject(String arg0) throws MissingResourceException {
		return null;
	}

	public void setParent(ResourceBundle parent) {
		super.setParent(parent);
	}

	public boolean isEmpty() {
		if (parent == null)
			return true;
		return ((BundleResourceBundle) parent).isEmpty();
	}

	public boolean isStemEmpty() {
		if (defaultRoot.equals(localeString))
			return false;
		if (parent == null)
			return true;
		return ((BundleResourceBundle) parent).isStemEmpty();
	}
	
}