/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.eclipse.core.supplement.localization;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import org.osgi.framework.Bundle;
import org.osgi.service.component.annotations.Component;

/**
 * Component that caches all property file for the bundle localization 
 * @author Mark Hoffmann
 * @since 07.11.2019
 */
@Component(immediate=true)
public class BundleLocalization implements org.eclipse.osgi.service.localization.BundleLocalization {
	
	private volatile Map<Bundle, ResourceBundleCache> resourceBundleCache = new ConcurrentHashMap<Bundle, ResourceBundleCache>();

	/* (non-Javadoc)
	 * @see org.eclipse.osgi.service.localization.BundleLocalization#getLocalization(org.osgi.framework.Bundle, java.lang.String)
	 */
	@Override
	public ResourceBundle getLocalization(Bundle bundle, String locale) {
		if (bundle == null) {
			return null;
		}
		String defaultLocale = Locale.getDefault().toString();
		if (locale == null) {
			locale = defaultLocale;
		}
		ResourceBundleCache rbc = resourceBundleCache.get(bundle);
		if (rbc == null) {
			rbc = new ResourceBundleCache(bundle);
			resourceBundleCache.put(bundle, rbc);
		}
		return rbc.getResourceBundle(locale, defaultLocale.equals(locale));
	}
	
}
