/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - Initial implementation 
 */
package org.gecko.eclipse.compatibility.equinox.config.api;

/**
 * Marker Service to show when the EquinoxConfiguration is properly filed. This was usually done by the EclipsePlattformStarter 
 * outside the Framework.
 * 
 * @author Juergen Albert
 *
 */
public interface EquinoxConfigInitializer {

}
